# Defining Region
variable "aws_region" {
  default = "ap-south-1"
}

# Defining CIDR Block for VPC
# Showing demo
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}
#defining s3 bucket
variable "bucket_name" {
  default = "gitlab-s3-demo"
}

variable "acl_value" {
    default = "private"
}
